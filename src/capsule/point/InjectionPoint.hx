package capsule.point;

import capsule.Injector;

interface InjectionPoint {
  public var field(default, null):String;
  public function applyInjection(target:Dynamic, injector:Injector):Dynamic;
}
