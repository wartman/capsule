package capsule.point;

import capsule.Injector;

class PostInjectionPoint extends MethodInjectionPoint {

  public var order(default, null):Int;

  public function new(field:String, args:Array<String>, order:Int) {
    super(field, args);
    this.order = order;
  }

}
