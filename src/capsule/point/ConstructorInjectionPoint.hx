package capsule.point;

import capsule.Injector;

class ConstructorInjectionPoint extends MethodInjectionPoint {

  public function new(args:Array<String>) {
    super('new', args);
  }

  public function createInstance<T>(type:Class<T>, injector:Injector):T {
    return Type.createInstance(type, gatherArgs(type, injector));
  }

}
