package capsule;

import capsule.point.ConstructorInjectionPoint;
import capsule.point.InjectionPoint;

class InjectorInfo {

  public var ctor:ConstructorInjectionPoint;
  public var fields:Array<InjectionPoint>;

  public function new(ctor:ConstructorInjectionPoint, fields:Array<InjectionPoint>) {
    this.ctor = ctor;
    this.fields = fields;
  }

}
