package capsule;

import haxe.rtti.Meta;
import haxe.macro.Expr;
import haxe.ds.ArraySort;
import capsule.point.*;
import capsule.provider.*;

class Injector {

  public static function getValueType(value:Dynamic):String {
    if (Std.is(value, String)) return 'String';
    if (Std.is(value, Class)) return Type.getClassName(value);
    if (Std.is(value, Enum)) return Type.getEnumName(value);
    var name = switch (Type.typeof(value)) {
      case TInt: 'Int';
      case TBool: 'Bool';
      case TClass(c): Type.getClassName(c);
      case TEnum(e): Type.getEnumName(e);
      default: null;
    }
    if (name != null) return name;
    throw 'Could not determine the type name of $value';
  }

  public var parent(default, null):Injector;
  private var bindings:Map<String, InjectorBinding<Dynamic>> = new Map();
  private var infos:Map<String, InjectorInfo> = new Map();

  public function new(?parent:Injector) {
    this.parent = parent;
  }

  public macro function bind(ethis:Expr, type:Expr, ?name:Expr):Expr {
    // Ensure type is not eliminated by DCE
    InjectorMacro.keep(type);

    // Get type identifier.
    var id = InjectorMacro.getExprType(type);

    // Get value type
    var type = InjectorMacro.getValueType(type);

    // Forward to the runtype method.
    return macro @:pos(ethis.pos) $ethis.bindType($id, $name, (null:$type));
  }

  public macro function bindTypeOf(ethis:Expr, value:Expr, ?name:Expr):Expr {
    var type = InjectorMacro.getValueId(value);
    return macro @:pos(ethis.pos) $ethis.bindType($type, $name).toValue($value);
  }

  public function bindRuntimeTypeOf(value:Dynamic, ?name:String):InjectorBinding<Dynamic> {
    return bindType(getValueType(value), name);
  }

  public function bindType<T:Dynamic>(type:String, ?name:String, ?value:T):InjectorBinding<T> {
    var key = getBindingKey(type, name);
    if (bindings.exists(key)) return cast bindings.get(key);
    var binding = new InjectorBinding(type, name);
    bindings.set(key, binding);
    return cast binding;
  }

  public macro function unbind(ethis:Expr, type:Expr, ?name:Expr):Expr {
    var type = InjectorMacro.getExprType(type);
    return macro @:pos(ethis.pos) $ethis.unbindType($type, $name);
  }

  public function unbindType(type:String, ?name:String):Void {
    #if debug
      if (!bindings.exists(getBindingKey(type, name))) {
        throw 'Error while removing an binding: No binding defined for type "$type", name "$name"';
      }
    #end
    bindings.remove(getBindingKey(type, name));
  }

  public macro function hasBinding(ethis:Expr, type:Expr, ?name:Expr):Expr {
    var type = InjectorMacro.getExprType(type);
    return macro @:pos(ethis.pos) $ethis.hasBindingForType($type, $name);
  }

  public function hasBindingForType(type:String, ?name:String):Bool {
    return getBindingForType(type, name) != null;
  }

  public macro function getBinding(ethis:Expr, type:Expr, ?name:Expr):Expr {
    var type = InjectorMacro.getExprType(type);
    return macro @:pos(ethis.pos) $ethis.getBindingForType($type, $name);
  }

  public function getBindingForType(type:String, name:String):InjectorBinding<Dynamic> {
    var binding = bindings.get(getBindingKey(type, name));
    if (binding != null && binding.provider != null) return binding;
    if (parent != null) parent.getBindingForType(type, name);
    return null;
  }

  /**
   * A shortcut to quickly set a key/value pair in your injector.
   *
   * The bound type will be inferred from `value`.
   *
   *    injector.set('foo', 'bar'); // Same as `injector.bind(String, 'foo').to('bar');`
   *    injector.get(String, 'foo'); // => 'bar'
   *
   */
  public macro function set(ethis:Expr, name:Expr, value:Expr):Expr {
    return macro @:pos(ethis.pos) $ethis.bindTypeOf($value, $name);
  }

  public macro function get(ethis:Expr, type:Expr, ?name:Expr):Expr {
    var type = InjectorMacro.getExprType(type);
    return macro @:pos(ethis.pos) $ethis.getValueForType($type, $name);
  }

  public function getValueForType(type:String, ?name:String):Dynamic {
    var binding = getBindingForType(type, name);
    if (binding != null) return binding.getValue(this);

    // if Array<Int> fails fall back to Array
    var index = type.indexOf('<');
    if (index > -1) binding = getBindingForType(type.substr(0, index), name);
    if (binding != null) return binding.getValue(this);

    return null;
  }

  public function injectInto(target:Dynamic):Void {
    var info = getInfo(Type.getClass(target));
    if (info == null) return;
    for (field in info.fields) {
      field.applyInjection(target, this);
    }
  }

  /**
   * Constructs an instance of `type` without satisfying its dependencies.
   */
  public macro function create(ethis:Expr, type:Expr):Expr {
    InjectorMacro.keep(type);
    return macro @:pos(ethis.pos) $ethis.rtCreate($type);
  }

  @:dox(hide)
  @:noCompletion
  public function rtCreate<T>(type:Class<T>):T {
    var info = getInfo(type);
    return info.ctor.createInstance(type, this);
  }

  public macro function build(ethis:Expr, type:Expr):Expr {
    InjectorMacro.keep(type);
    return macro @:pos(ethis.pos) $ethis.rtBuild($type);
  }

  @:dox(hide)
  @:noCompletion
  public function rtBuild<T>(type:Class<T>):T {
    var instance = rtCreate(type);
    injectInto(instance);
    return instance;
  }

  public function createChildInjector() {
    return new Injector(this);
  }

  private function getInfo(cls:Class<Dynamic>):InjectorInfo {
    var type = Type.getClassName(cls);
    if (infos.exists(type)) return infos.get(type);
    var info = createInfoFor(cls);
    infos.set(type, info);
    return info;
  }

  private function createInfoFor(cls:Class<Dynamic>):InjectorInfo {
    var info = new InjectorInfo(null, []);
    addClassToInfo(cls, info, []);
    ArraySort.sort(info.fields, function (p1, p2) {
      var post1 = Std.instance(p1, PostInjectionPoint);
      var post2 = Std.instance(p2, PostInjectionPoint);
      return switch ([post1, post2]) {
        case [null, null]: 0;
        case [null, _]: -1;
        case [_, null]: 1;
        default: post1.order - post2.order;
      }
    });
    if (info.ctor == null) info.ctor = new ConstructorInjectionPoint([]);
    return info;
  }

  private function addClassToInfo(cls:Class<Dynamic>, info:InjectorInfo, injected:Array<String>):Void {
    var meta = Meta.getType(cls);

    #if debug
      if (meta != null && Reflect.hasField('interface')) {
        throw 'Interfaces can\'t be used as instantiatable classes.';
      }
    #end

    var fields:Array<Array<String>> = cast meta.rtti;

    if (fields != null) {
      for (field in fields) {
        var name = field[0];
        if (injected.indexOf(name) > -1) continue;
        injected.push(name);

        if (field.length == 3) {
          info.fields.push(new PropertyInjectionPoint(name, field[1], field[2]));
        } else if (name == 'new') {
          info.ctor = new ConstructorInjectionPoint(field.slice(2));
        } else {
          var orderStr = field[1];
          if (orderStr == '') {
            info.fields.push(new MethodInjectionPoint(name, field.slice(2)));
          } else {
            var order = Std.parseInt(orderStr);
            info.fields.push(new PostInjectionPoint(name, field.slice(2), order));
          }
        }
      }
    }

    var superClass = Type.getSuperClass(cls);
    if (superClass != null) addClassToInfo(superClass, info, injected);
  }

  private function getBindingKey(type:String, name:String):String {
    if (name == null) name = '';
    return '$type#$name';
  }

}
