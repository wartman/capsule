package capsule.provider;

import capsule.Injector;

class FactoryProvider<T> implements DependencyProvider<T> {

  private var factory:Injector->T;

  public function new(factory:Injector->T) {
    this.factory = factory;
  }

  public function getValue(injector:Injector):T {
    return this.factory(injector);
  }

  #if debug
    public function toString():String {
      return 'factory';
    }
  #end

}
