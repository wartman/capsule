package capsule.provider;

import capsule.Injector;
import capsule.InjectorBinding;

class OtherBindingProvider<T> implements DependencyProvider<T> {

  private var binding:InjectorBinding<T>;

  public function new(binding:InjectorBinding<T>) {
    this.binding = binding;
  }

  public function getValue(injector:Injector):T {
    return binding.getValue(injector);
  }

  #if debug
    public function toString():String {
      return binding.toString();
    }
  #end

}
