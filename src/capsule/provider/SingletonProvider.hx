package capsule.provider;

import capsule.Injector;

class SingletonProvider<T> implements DependencyProvider<T> {

  private var type:Class<T>;
  private var value:T;

  public function new(type:Class<T>) {
    this.type = type;
  }

  public function getValue(injector:Injector):T {
    if (value == null) {
      value = injector.rtCreate(type);
      injector.injectInto(value);
    }
    return value;
  }

  #if debug
    public function toString():String {
      return 'class ${Type.getClassName(type)}';
    }
  #end

}
