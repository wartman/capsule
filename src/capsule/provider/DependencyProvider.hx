package capsule.provider;

import capsule.Injector;

interface DependencyProvider<T> {
  public function getValue(injector:Injector):T;
  #if debug
    public function toString():String;
  #end
}
