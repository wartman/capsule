package capsule.provider;

import capsule.Injector;

class ClassProvider<T> implements DependencyProvider<T> {

  private var type:Class<T>;

  public function new(type:Class<T>) {
    this.type = type;
  }

  public function getValue(injector:Injector):T {
    return injector.rtBuild(type);
  }

  #if debug
    public function toString():String {
      return 'class ${Type.getClassName(type)}';
    }
  #end

}
