package capsule;

import haxe.macro.Expr;
import capsule.provider.*;

class InjectorBinding<T> {

  public var type:String;
  public var name:String;
  public var injector:Injector;
  public var provider:DependencyProvider<T>;

  public function new(type:String, name:String) {
    this.type = type;
    this.name = name;
  }

  public function getValue(injector:Injector):T {
    if (this.injector != null) injector = this.injector;
    if (provider != null) return provider.getValue(injector);
    var parent = injector.getBindingForType(type, name);
    if (parent != null) return parent.getValue(injector);
    return null;
  }

  /**
   * Automatically bind a value based on the type of expression.
   *
   * If a function is passed, will bind using `toFunction`. A class will be bound using `toClass`.
   * All other values are bound with `toValue`.
   */
  public macro function to(ethis:Expr, target:Expr):Expr {
    return switch (haxe.macro.Context.typeof(target)) {
      case haxe.macro.Type.TFun(_, _): macro @:pos(ethis.pos) $ethis.toFactory($target);
      case haxe.macro.Type.TType(_, _): macro @:pos(ethis.pos) $ethis.toClass($target);
      default: macro @:pos(ethis.pos) $ethis.toValue($target);
    }
  }

  public function toValue(value:T):InjectorBinding<T> {
    return toProvider(new ValueProvider(value));
  }

  public function toFactory(factory:Injector->T):InjectorBinding<T> {
    return toProvider(new FactoryProvider(factory));
  }

  public macro function toClass(ethis:Expr, type:Expr):Expr {
    InjectorMacro.keep(type);
    return macro @:pos(ethis.pos) $ethis.rtToClass($type);
  }

  @:dox(hide)
  @:noCompletion
  public function rtToClass(type:Class<T>):InjectorBinding<T> {
    return toProvider(new ClassProvider<T>(type));
  }

  public macro function toSingleton(ethis:Expr, type:Expr):Expr {
    InjectorMacro.keep(type);
    return macro @:pos(ethis.pos) $ethis.rtToSingleton($type);
  }

  @:dox(hide)
  @:noCompletion
  public function rtToSingleton(type:Class<T>):InjectorBinding<T> {
    return toProvider(new SingletonProvider<T>(type));
  }

  public function toBinding(binding:InjectorBinding<Dynamic>):InjectorBinding<T> {
    return toProvider(new OtherBindingProvider(binding));
  }

  public function asSingleton():InjectorBinding<T> {
    return rtToSingleton(cast Type.resolveClass(type));
  }

  public function toProvider(provider:DependencyProvider<T>):InjectorBinding<T> {
    #if debug
      if (this.provider != null && provider != null) {
        trace(
          'Warning: Injector contains ${this.toString()}. Attempting to overwrite this with biding for ' +
          '${provider.toString()}.\nIf you have overwritten this binding intentionally you should use' +
          '`injector.unbind` prior to binding your replacement to avoid this message.'
        );
      }
    #end
    this.provider = provider;
    return this;
  }

}
