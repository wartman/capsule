import haxe.unit.TestRunner;

class TestAll {

  public static function main() {
    var runner = new TestRunner();
    runner.add(new capsule.InjectorTest());
    runner.run();
  }

}
