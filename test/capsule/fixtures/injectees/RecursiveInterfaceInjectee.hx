package capsule.fixtures.injectees;

import capsule.fixtures.types.Interface1;

class RecursiveInterfaceInjectee implements Interface1 {

  @inject public var interfaceInjectee:InterfaceInjectee;

  public function new() {}

}
