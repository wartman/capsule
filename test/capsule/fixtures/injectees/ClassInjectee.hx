package capsule.fixtures.injectees;

import capsule.fixtures.types.Class1;

class ClassInjectee {

  @inject public var property:Class1;
  public var someProperty:Bool;

  public function new() {
    someProperty = false;
  }

  @post(1) public function onPost() {
    someProperty = true;
  }

}
