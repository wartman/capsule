package capsule.fixtures.injectees;

import capsule.fixtures.types.Class1;
import capsule.fixtures.types.Class2;

class InheritanceInjectee extends ClassInjectee {

  @inject public var property2:Class2;
  public var extraProperty:Bool;

  public function new() {
    super();
    extraProperty = false;
  }

  @post(2) public function doExtraThings() {
    extraProperty = true;
  }

}
