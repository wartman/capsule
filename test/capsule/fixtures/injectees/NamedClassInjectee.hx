package capsule.fixtures.injectees;

import capsule.fixtures.types.Class1;

class NamedClassInjectee {

  public static var NAME = 'name';

  @inject('name') public var property:Class1;

  public function new() {}

}
