package capsule.fixtures.injectees;

import capsule.fixtures.types.Class1;

class TypedefInjectee {

  @inject public var property:Typedef1;
  public var someProperty:Bool;

  public function new() {
    someProperty = false;
  }

  @post(1) public function doThings() {
    someProperty = true;
  }

}

typedef Typedef1 = Class1;
