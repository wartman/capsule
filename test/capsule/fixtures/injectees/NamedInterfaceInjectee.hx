package capsule.fixtures.injectees;

import capsule.fixtures.types.Interface1;

class NamedInterfaceInjectee {

  public static var NAME = 'name';

  @inject('name') public var property:Interface1;

  public function new() {}

}
