package capsule.fixtures.injectees;

import capsule.fixtures.types.Interface1;

class InterfaceInjectee {

  @inject public var property:Interface1;

  public function new() {}

}
