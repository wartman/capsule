package capsule.fixtures.injectees;

class TypeParamInjectee {

  @inject public var names:Array<String>;
  @inject public var numbers:Array<Int>;
  @inject('cities') public var cities:Array<String>;
  @inject('populations') public var populations:Array<Int>;

  @inject('','','cities','populations')
  public function testMethodInjection(names:Array<String>, numbers:Array<Int>, cities:Array<String>, populations:Array<Int>) {
    // Just test that no exception is thrown.
  }

  public function new() {}

}
