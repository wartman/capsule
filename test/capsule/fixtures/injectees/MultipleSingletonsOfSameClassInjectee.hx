package capsule.fixtures.injectees;

import capsule.fixtures.types.Interface1;
import capsule.fixtures.types.Interface2;

class MultipleSingletonsOfSameClassInjectee {

  @inject public var property1:Interface1;
  @inject public var property2:Interface2;

  public function new() {}

}
