package capsule;

import haxe.unit.TestCase;
import capsule.fixtures.injectees.*;
import capsule.fixtures.injectees.TypedefInjectee;
import capsule.fixtures.types.*;
using Type;

class InjectorTest extends TestCase {

  private var injector:Injector;

  override public function setup() {
    injector = new Injector();
  }

  override public function tearDown() {
    injector = null;
  }

  public function testUnbind() {
    var injectee = new ClassInjectee();
    var value = new Class1();
    injector.bind(Class1).toValue(value);
    injector.unbind(Class1);

    try {
      injector.injectInto(injectee);
    } catch (e:Dynamic) {}

    assertEquals(null, injectee.property);
  }

  public function testInjectsBoundValueIntoAllInjectees() {
    var value = new Class1();
    injector.bind(Class1).toValue(value);

    var injectee1 = new ClassInjectee();
    injector.injectInto(injectee1);

    var injectee2 = new ClassInjectee();
    injector.injectInto(injectee2);

    assertEquals(injectee1.property, injectee2.property);
  }

  public function testBoundValueByInterface() {
    var injectee = new InterfaceInjectee();
    var value = new Class1();

    injector.bind(Interface1).toValue(value);
    injector.injectInto(injectee);

    assertSameClass(value, injectee.property);
  }

  public function testQuickSet() {
    injector.set('foo', 'foo');
    injector.set('bin', 1);

    assertEquals('foo', injector.get(String, 'foo'));
    assertEquals(1, injector.get(Int, 'bin'));
  }

  public function testBindingToMethodAutomaticallyDetectsType() {
    injector.bind(String, 'foo').to('foo');
    injector.bind(String, 'bar').to(function (injector) {
      return injector.get(String, 'foo');
    });
    injector.bind(Interface1).to(Class1);

    assertEquals('foo', injector.get(String, 'foo'));
    assertEquals('foo', injector.get(String, 'bar'));
    assertEquals(Class1.getClassName(), Type.getClass(injector.get(Interface1)).getClassName());
  }

  public function testBindNamedValueByInterface() {
    var injectee = new NamedInterfaceInjectee();
    var value = new Class1();

    injector.bind(Interface1, NamedInterfaceInjectee.NAME).toValue(value);
    injector.injectInto(injectee);

    assertSameClass(value, injectee.property);
  }

  public function testBindFalsyValue() {
    var injectee = new StringInjectee();
    var value = 'test';

    injector.bind(String).to(value);
    injector.injectInto(injectee);

    assertEquals(value, injectee.property);
  }

  public function testBoundValueIsNotInjectedInto() {
    var injectee = new RecursiveInterfaceInjectee();
    var value = new InterfaceInjectee();

    injector.bind(InterfaceInjectee).to(value);
    injector.injectInto(injectee);

    assertEquals(null, value.property);
  }

  public function testBindMultipleInterfacesToOneSingletonClass() {
    var injectee = new MultipleSingletonsOfSameClassInjectee();

    injector.bind(Interface1).toSingleton(Class1);
    injector.bind(Interface2).toSingleton(Class1);

    injector.injectInto(injectee);

    assertFalse(null == injectee.property1);
    assertFalse(null == injectee.property2);

    var same = untyped (injectee.property1 == injectee.property2);
    assertFalse(same);
  }

  public function testBindClassesAreNotSameObject() {
    injector.bind(Class1).toClass(Class1);

    var injectee1 = new ClassInjectee();
    injector.injectInto(injectee1);

    var injectee2 = new ClassInjectee();
    injector.injectInto(injectee2);

    assertFalse(null == injectee1.property);
    assertFalse(null == injectee2.property);
    assertFalse(injectee1.property == injectee2.property);
  }

  public function testBindInheritedClass() {
    injector.bind(Class1).toClass(Class1);
    injector.bind(Class2).toClass(Class2);

    var injectee1 = new InheritanceInjectee();
    injector.injectInto(injectee1);

    var injectee2 = new InheritanceInjectee();
    injector.injectInto(injectee2);

    assertFalse(null == injectee1.property);
    assertFalse(null == injectee1.property2);
    assertTrue(injectee1.someProperty);
    assertTrue(injectee1.extraProperty);
    assertFalse(injectee1.property == injectee2.property);
    assertFalse(injectee1.property2 == injectee2.property2);
  }

  public function testBindTypedef() {
    injector.bind(Typedef1).toClass(Typedef1);
    injector.bind(TypedefInjectee).toClass(TypedefInjectee);

    assertTrue(injector.hasBinding(Typedef1));
    assertTrue(injector.hasBinding(Class1));

    var injectee = new TypedefInjectee();
    injector.injectInto(injectee);

    assertFalse(null == injectee.property);
  }

  public function bindTypeParams() {
    injector.bind('Array<String>').to(['Bill', 'Ted']);
    injector.bind('Array<Int>').to([0,1,2]);
    injector.bind('Array<String>', 'cities').to(['London', 'New York', 'Minneapolis']);
    injector.bind('Array<Int>', 'populations').to([8416535, 4840600, 2021200]);

    var injectee = new TypeParamInjectee();
    injector.injectInto(injectee);

    assertEquals('Bill', injectee.names[0]);
    assertEquals(0, injectee.numbers[0]);
    assertEquals('London', injectee.cities[0]);
    assertEquals(8416535, injectee.populations[0]);
  }

  public function testBindInterface() {
    injector.bind(Interface1).toClass(Class1);

    var injectee = new InterfaceInjectee();
    injector.injectInto(injectee);

    assertFalse(null == injectee.property);
    assertEquals(Class1.getClassName(), injectee.property.getClass().getClassName());
  }

  public function testBindNamedClass() {
    injector.bind(Class1, NamedClassInjectee.NAME).to(Class1);

    var injectee = new NamedClassInjectee();
    injector.injectInto(injectee);

    assertFalse(null == injectee.property);
    assertEquals(Class1.getClassName(), injectee.property.getClass().getClassName());
  }

  public function testBindNamedClassByInterface() {
    injector.bind(Interface1, NamedInterfaceInjectee.NAME).to(Class1);
    
    var injectee = new NamedInterfaceInjectee();
    injector.injectInto(injectee);

    assertFalse(null == injectee.property);
    assertEquals(Class1.getClassName(), injectee.property.getClass().getClassName());
  }

  public function testBindSingleton() {
    var injectee1 = new ClassInjectee();
    var injectee2 = new ClassInjectee();

    injector.bind(Class1).asSingleton();

    injector.injectInto(injectee1);
    assertFalse(null == injectee1.property);

    injector.injectInto(injectee2);
    assertFalse(null == injectee2.property);

    assertEquals(injectee1.property, injectee2.property);
  }

  public function testBindSingletonOfInterface() {
    var injectee1 = new InterfaceInjectee();
    var injectee2 = new InterfaceInjectee();

    injector.bind(Interface1).toSingleton(Class1);

    injector.injectInto(injectee1);
    assertFalse(null == injectee1.property);

    injector.injectInto(injectee2);
    assertFalse(null == injectee2.property);

    assertEquals(injectee1.property, injectee2.property);
  }

  // -- LINE 282 on https://github.com/massiveinteractive/minject/blob/master/src/test/minject/InjectorTest.hx

  private function assertSameClass(expected:Dynamic, actual:Dynamic) {
    assertEquals(Type.getClass(expected).getClassName(), Type.getClass(actual).getClassName());
  }

}
