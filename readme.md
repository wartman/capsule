Capsule
=======

Capsule is based on minject, and has a similar syntax and feature set.

Usage
-----

Here's a simple example:

```

class Main {
  
  public static function main() {
    var injector = new capsule.Injector();
    injector.bind(String, 'foo').to('foo');
    trace(injector.get(String, 'foo'));
  }

}

```

API
---

Capsule's API is simple.

```

// Bind a class (inferred as a `toClass` binding):
injector.bind(MyClass);

// Bind an interface to an implementation:
injector.bind(MyInterface).toClass(MyImplementation);

// Bind a value to a type:
injector.bind(String).toValue('bar');

// Bind a value with a name to a type:
injector.bind(String, 'foo').toValue('foo');

// Capsule can infer what kind of binding you want based on type, so you
// can rewrite the above like:
injector.bind(MyInterface).to(MyImplementation);
injector.bind(String, 'foo').to('foo');
// ... and they'll work correctly.

// Resolve a binding:
var instance = injector.get(MyInterface); // Will return an instance of `MyImplementation`.
var foo = injector.get(String, 'foo'); // returns 'foo'

```

Dependecy bindings can be injected using property, method or constructor injection via metadata:

```

class MyImplementation implements MyInterface {

  // The below will be injected with `injector.get(BinClass)`
  @inject public var bin:BinClass
  
  // The below will injected with `injector.get(String, 'foo')`:
  @inject('foo') public var foo:String;

  // The below will be injected with `[injector.get(FooClass), injector.get(BarClass, 'bar')]`
  @inject('', 'bar') public function new(foo:FooClass, bar:BarClass) {
    // do things
  }

  // You can register methods to fire after all injections are complete with `@post` meta.
  // A param will be used to set the method's priority, but is not required.
  @post(1) public function afterInjection() {
    trace(foo); // => 'foo'
  }

}

```

You have a few options to inject dependencies. The first is to use `injectInto` on an existing instance:

```

var obj = new Thing()
injector.injectInto(obj);

```

`injector.build` will create an object for you and also handle constructor injection:

```

var obj = injector.build(MyImplementation);

```

Or you can bind the object to the injector and retrieve it with `get`:

```

injector.bind(MyImplementation);
var obj = injector.get(MyImplementation);

```

Advanced
--------

###Child Injectors

Child injectors are dependent on their parent injector, but can override dependencies as needed.

```

var injector = new capsule.Injector();
injector.bind(String, 'foo').to('foo');
injector.bind(String, 'bar').to('bar');

var child = injector.createChildInjector();
child.bind(String, 'bar').to('override');

child.get(String, 'foo'); // => 'foo';
child.get(String, 'bar'); // => 'override';

```

*MORE TO COME*
